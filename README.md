# Sample Parser

The aim of this project is to give the basic setup for a parser using the python library developed for
[NOMAD CoE](http://nomad-coe.eu)

The setup is expected to be improved as we find better ways to write the parsers.
For your parser rename parser/sample-parser to parser/parser-YourCode and likewise fix
the code.

The python-common and nomad-meta-info repositories are expected to be checked out one level
higher as this repository.
This is automatically the case if you clone nomad-lab-base (the parsers are in parsers subdirectory).

For more information on how to write a parser see the [wiki](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-lab-base/wikis/how-to-write-a-parser).
